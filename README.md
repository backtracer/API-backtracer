<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Parent Project 

The parent project is [Backtracer/Backtracer](https://gitlab.com/backtracer/backtracer)

## Installation

```bash
$ npm install -g @nestjs/cli
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Controllers and Routes 

### Geocoding

- `/geocoding`
  - [GET] `/geocoding/autocomplete`
  
    Return an observable linked with [French Address API](https://adresse.data.gouv.fr/api-doc/adresse). It filter results to only return address in the region "Ile de France". It permit to purpose an autocompletion in front's addresses imputs.

- `/agencies`
  - [GET] `/agencies`
    
    Return all agencies

- `/gtfs`
  - [GET] `/gtfs` | Query parameters (partition sizes): `routeTypes` ; `agency` ; `calendar` ; `calendarDate` ; `route` ; `trip` ; `stop` ; `stopTime` ; `transfer` ; `postscript` (1 or 0) 
    
    Import the GTFS file in the database. If you don't want to import the entire database, don't set the parameter or set it to 0. For the first import, we recommand to import with ALL parameters or import them in the previous order. Otherwise, you can meet Foreign Key errors.
- `/itinerary`
  - [GET] `/itinerary` | Query parameters : `departure` ; `arrival` ➡️ format ("`2.439834;48.625578`") 

    Return an itinerary using [Navitia format](https://canaltp.github.io/navitia-playground/play.html?request=https%3A%2F%2Fapi.navitia.io%2Fv1%2Fjourneys%3Ffrom%26to).


- `/route-types`
  - [GET] `/route-types`

    Return all Route Types

- `/routes`
  - [GET] `/routes`

    Return all Routes
  - [GET] `/routes/:routeId` where `:routeId` is a routeId 

    Get route informations by routeId
  - [GET] `/routes/:routeId/stops` where `:routeId` is a routeId

    Get stops by routeId
  - [GET] `/routes/:routeId/trips` where `:routeId` is a routeId

    Get trips by routeId
  - [GET] `/routes/:routeId/live` where `:routeId` is a routeId

    Get current positions of train of a specified routeId


- `/stops`
  - [GET] `/stops` | Query Parameters : `lat` a decimal number for latitude ; `lon` a decimal number for longitude 

    Return the closest stop for a position
    


## License

Nest is [MIT licensed](LICENSE).


