import { Controller, Get, Query } from '@nestjs/common';
import { GeocodingService } from './geocoding.service';

@Controller('geocoding')
export class GeocodingController {
    constructor(private geocodingService: GeocodingService) {}

    @Get('autocomplete')
    autocomplete(@Query('q') q: string) {
        return this.geocodingService.autocomplete(q)
    }
}
