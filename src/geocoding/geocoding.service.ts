import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { map, Observable} from "rxjs";

@Injectable()
export class GeocodingService {
    constructor(private httpService: HttpService) {}

    autocomplete(query: string): Observable<any> {
        let depList=["75","77","78","91","92","93","94","95"]
        return this.httpService.get(encodeURI(`https://api-adresse.data.gouv.fr/search/?q=${query}&limit=30&autocomplete=1&lat=48.856614&lon=2.3522219`)).pipe(map(response => {
            let result = {type:	"FeatureCollection",features:[]};
            for (let ft of response.data.features){
                if(depList.includes(ft.properties.postcode.substring(0,2)) ){
                    result.features.push(ft)
                }
            }
            return result;
        }))
    }
}
