import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { map, Observable } from 'rxjs';

@Injectable()
export class ItineraryService {

    constructor(
        private httpService: HttpService
    ){}

    public generateItinerary(from:number,to:number): Observable<any>{
        let url:string="https://api.navitia.io/v1/journey?"
        return this.httpService.get(encodeURI(`https://api.navitia.io/v1/journeys?from=${from}&to=${to}`), {
            headers: {
              authorization: process.env.API_NAVITIA_TOKEN
            }
        }).pipe(map(response => response.data))
    }
}
