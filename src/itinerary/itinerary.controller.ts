import * as fs from 'fs';
import { Controller, Get, Query } from '@nestjs/common';
import { ItineraryService } from './itinerary.service';

@Controller('itinerary')
export class ItineraryController {

  constructor(
    private itineraryService: ItineraryService
  ) {}


  @Get()
  public async getItinerary(@Query('departure') departure: number, @Query('arrival') arrival : number) {
    console.log("Calcul d'itinéraire de "+departure+" à "+arrival);  
    return this.itineraryService.generateItinerary(departure,arrival);
  }

  @Get("bouchon")
  public async getBouchonItinerary(@Query('departure') departure: number, @Query('arrival') arrival : number) {
    console.log("Bouchon d'itinéraire");
    return JSON.parse(fs.readFileSync(`${__dirname}/../../data/bouchons_itineraires/responseItinerary.json`, 'utf8'));
  }

}

