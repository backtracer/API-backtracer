import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ItineraryController } from './itinerary.controller';
import { ItineraryService } from './itinerary.service';

@Module({
  imports: [HttpModule],
  providers: [ItineraryService],
  controllers: [ItineraryController]
})
export class ItineraryModule {}
