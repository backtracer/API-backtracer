import { Test, TestingModule } from '@nestjs/testing';
import { RouteTypesController } from './route-types.controller';

describe('RouteTypesController', () => {
  let controller: RouteTypesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RouteTypesController],
    }).compile();

    controller = module.get<RouteTypesController>(RouteTypesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
