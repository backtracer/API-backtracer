import { Module } from '@nestjs/common';
import { RouteTypesController } from './route-types.controller';
import { RouteTypesService } from './route-types.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RouteType } from 'src/model/RouteType.entity';

@Module({
  imports: [
      TypeOrmModule.forFeature([RouteType])
  ],
  controllers: [RouteTypesController],
  providers: [RouteTypesService],
  exports: [RouteTypesService]
})
export class RouteTypesModule {}
