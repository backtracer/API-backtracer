import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RouteType } from 'src/model/RouteType.entity';
import { DeepPartial, Repository } from 'typeorm';

@Injectable()
export class RouteTypesService {
    constructor(
      @InjectRepository(RouteType)
      private routeTypesRepository: Repository<RouteType>){}

      public async saveMany(routeTypes: DeepPartial<RouteType>[]): Promise<RouteType[]> {
        return this.routeTypesRepository.save(routeTypes);
      }

      public async getAll(){
          return this.routeTypesRepository.find();
      }

}
