import { Controller, Get } from '@nestjs/common';
import { RouteTypesService } from './route-types.service';

@Controller('route-types')
export class RouteTypesController {
	constructor(private routeTypesService: RouteTypesService){}

	@Get()
	public async getAll() {
	  return this.routeTypesService.getAll();
	}
}
