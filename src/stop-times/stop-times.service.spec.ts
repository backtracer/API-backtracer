import { Test, TestingModule } from '@nestjs/testing';
import { StopTimesService } from './stop-times.service';

describe('StopTimesService', () => {
  let service: StopTimesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StopTimesService],
    }).compile();

    service = module.get<StopTimesService>(StopTimesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
