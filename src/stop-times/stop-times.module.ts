import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StopTime } from 'src/model/StopTime.entity';
import { StopTimesService } from './stop-times.service';

@Module({
  imports: [TypeOrmModule.forFeature([StopTime])],
  controllers: [],
  providers: [StopTimesService],
  exports: [StopTimesService],
})
export class StopTimesModule {}
