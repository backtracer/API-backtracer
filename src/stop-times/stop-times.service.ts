import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { StopTime } from 'src/model/StopTime.entity';
import { DeepPartial, In, Repository } from 'typeorm';

@Injectable()
export class StopTimesService {
  constructor(
    @InjectRepository(StopTime)
    private stopTimesRepository: Repository<StopTime>,
  ) {}

  public async findByTripIds(
    tripId: string[],
  ): Promise<StopTime[] | undefined> {
    return this.stopTimesRepository.find({ tripId: In(tripId) });
  }

  public async findByTripIdAndStopId(tripId:string,stopSequence:number):Promise<StopTime>{
    return this.stopTimesRepository.findOne({tripId:tripId,stopSequence:stopSequence});
  }

  public async findByTripId(tripId: string): Promise<StopTime[] | undefined> {
    return this.stopTimesRepository.find({
      where: { tripId },
      order: { stopSequence: 'ASC' },
    });
  }

  public async findByTripIdOp(tripId: string): Promise<StopTime[] | undefined> {
    return this.stopTimesRepository.find({
      select: ['stopId'],
      where: { tripId },
      order: { stopSequence: 'ASC' },
    });
  }

  public async getStopTimesFromStopIdAndTripIds(
    stopId: string,
    tripsIds: string[],
  ): Promise<StopTime[] | undefined> {
    return this.stopTimesRepository.find({
      where: { tripId: In(tripsIds), stopId: stopId },
      order: { stopSequence: 'ASC' },
    });
  }
  
  public saveOne(stopTime: DeepPartial<StopTime>): Promise<StopTime> {
		return this.stopTimesRepository.save(stopTime);
	}

  public async saveMany(stopTimes: DeepPartial<StopTime>[]): Promise<StopTime[]> {
    return this.stopTimesRepository.save(stopTimes);
  }
}
