import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Between, Connection, In, Repository, DeepPartial } from 'typeorm';
import Decimal from 'decimal.js';
import { TripsService } from '../trips/trips.service';
import { StopTimesService } from '../stop-times/stop-times.service';
import { Stop } from 'src/model/Stop.entity';

@Injectable()
export class StopsService {
  constructor(
    @InjectConnection() private connection: Connection,
    @InjectRepository(Stop) private stopsRepository: Repository<Stop>,
    private tripsService: TripsService,
    private stopTimesService: StopTimesService,
  ) {}

  public async findById(stopId: string): Promise<Stop | undefined> {
    return this.stopsRepository.findOne({ stopId });
  }

  public async findManyByIds(stopIds: string[]): Promise<Stop[] | undefined> {
    return this.stopsRepository.find({ stopId: In(stopIds) });
  }

  public async getStopsByRouteId(routeId: string): Promise<Stop[]> {
    const idStops: Set<string> = new Set();

    const trips = await this.tripsService.findByRouteId(routeId);

    for (const trip of trips) {
      const stopTimes = await this.stopTimesService.findByTripIdOp(trip.tripId);
      for (const stopTime of stopTimes) {
        if(!idStops.has(stopTime.stopId)){
          idStops.add(stopTime.stopId);
        }
      }
    }

    return await this.findManyByIds(Array.from(idStops.values()));
  }

  async findClosetStopFromLatLon(lat: number, lon: number): Promise<Stop> {
    let closetStop: Stop[] = [];

    let minLat = new Decimal(lat);
    let maxLat = new Decimal(lat);
    let minLon = new Decimal(lon);
    let maxLon = new Decimal(lon);

    do {
      closetStop = await this.stopsRepository.find({
        stopLat: Between(minLat.toNumber(), maxLat.toNumber()),
        stopLon: Between(minLon.toNumber(), maxLon.toNumber()),
      });
      minLat = minLat.minus(0.00001);
      maxLat = maxLat.plus(0.00001);
      minLon = minLon.minus(0.00001);
      maxLon = maxLon.plus(0.00001);
    } while (closetStop.length < 1);

    return closetStop[0];
  }

  saveOne(stop: DeepPartial<Stop>): Promise<Stop> {
		return this.stopsRepository.save(stop);
	}

  async saveMany(stops: DeepPartial<Stop>[]): Promise<Stop[]> {
		return this.stopsRepository.save(stops);
	}

}
