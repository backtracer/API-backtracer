import { Controller, Get, Query } from '@nestjs/common';
import { StopsService } from './stops.service';

@Controller('stops')
export class StopsController {
  constructor(private stopsService: StopsService) {}

  @Get()
  async getClosetStops(@Query('lat') lat: number, @Query('lon') lon: number) {
    if (lat && lon) {
      return this.stopsService.findClosetStopFromLatLon(lat, lon);
    }
  }
}
