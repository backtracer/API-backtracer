import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StopsService } from './stops.service';
import { StopsController } from './stops.controller';
import { TripsModule } from 'src/trips/trips.module';
import { StopTimesModule } from 'src/stop-times/stop-times.module';
import { Stop } from 'src/model/Stop.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Stop]), TripsModule, StopTimesModule],
  controllers: [StopsController],
  providers: [StopsService],
  exports: [StopsService],
})
export class StopsModule {}
