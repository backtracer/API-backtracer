import { Controller, Get } from '@nestjs/common';
import { AgenciesService } from './agencies.service';
import { Agency } from '../model/Agency.entity';

@Controller('agencies')
export class AgenciesController {
  constructor(private agenciesService: AgenciesService) {}

  @Get()
  getAllAgencies(): Promise<Agency[]> {
    return this.agenciesService.findAll();
  }
}
