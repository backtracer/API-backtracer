import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Agency } from '../model/Agency.entity';
import { DeepPartial, Repository } from 'typeorm';

@Injectable()
export class AgenciesService {
	constructor(
		@InjectRepository(Agency) private agencyRepository: Repository<Agency>,
	) {
	}

	findAll(): Promise<Agency[]> {
		return this.agencyRepository.find({order: {agencyName: 1}});
	}

	async findById(id:string):Promise<Agency>{
		return this.agencyRepository.findOne(id);
	}

	saveOne(agency: DeepPartial<Agency>): Promise<Agency> {
		return this.agencyRepository.save(agency);
	}

	async saveMany(agencies: DeepPartial<Agency>[]): Promise<Agency[]> {
		return this.agencyRepository.save(agencies);
	}
}
