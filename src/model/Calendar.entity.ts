import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm';
import { CalendarDates } from './CalendarDates.entity';
import { Trip } from './Trip.entity';

export enum DispoService {
  DISPO = 0,
  INDISPO = 1,
}

@Entity('calendar', { schema: 'public' })
export class Calendar {
  static fromCSVForTypeORM(values: any[]): Partial<Calendar> {
    return {
      serviceId: values[0],
      monday: values[1],
      tuesday: values[2],
      wednesday: values[3],
      thursday: values[4],
      friday: values[5],
      saturday: values[6],
      sunday: values[7],
      startDate: new Date(values[8].substring(0,4), values[8].substring(4,6), values[8].substring(6)),
      endDate: new Date(values[9].substring(0,4),values[9].substring(4,6),values[9].substring(6))
    };
  }

  @PrimaryColumn('character varying', {
    name: 'service_id',
    length: 255,
  })
  serviceId: string;

  @Column({
    type: 'enum',
    enum: DispoService,
    default: DispoService.DISPO,
    name: 'monday',
  })
  monday: DispoService;

  @Column({
    type: 'enum',
    enum: DispoService,
    default: DispoService.DISPO,
    name: 'tuesday',
  })
  tuesday: DispoService;

  @Column({
    type: 'enum',
    enum: DispoService,
    default: DispoService.DISPO,
    name: 'wednesday',
  })
  wednesday: DispoService;

  @Column({
    type: 'enum',
    enum: DispoService,
    default: DispoService.DISPO,
    name: 'thursday',
  })
  thursday: DispoService;

  @Column({
    type: 'enum',
    enum: DispoService,
    default: DispoService.DISPO,
    name: 'friday',
  })
  friday: DispoService;

  @Column({
    type: 'enum',
    enum: DispoService,
    default: DispoService.DISPO,
    name: 'saturday',
  })
  saturday: DispoService;

  @Column({
    type: 'enum',
    enum: DispoService,
    default: DispoService.DISPO,
    name: 'sunday',
  })
  sunday: DispoService;

  @Column('date', { name: 'start_date' })
  startDate: Date;

  @Column('date', { name: 'end_date' })
  endDate: Date;

  @OneToMany(() => Trip, (trip) => trip.calendar)
  trips: Trip[];

  // @OneToMany(() => CalendarDates, (clDate) => clDate.calendar)
  // calendarDates: CalendarDates[];
}
