import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { Route } from './Route.entity';
import { Calendar } from './Calendar.entity';
import { CalendarDates } from './CalendarDates.entity';
import { StopTime } from './StopTime.entity';

@Entity()
export class Trip {

	static fromCSVForTypeORM(values: string[]): Partial<Trip> {
		return {
			routeId: !values[0].trim()?null:values[0],
			serviceId: !values[1].trim()?null:values[1],
      tripId: !values[2].trim()?null:values[2],
      tripHeadsign : !values[3].trim()?null:values[3],
      tripShortName: !values[4].trim()?null:values[4],
      directionId: Number(values[5]),
      //blockId: values[6],
      //shapeId: values[7],
      wheelchairAccessible: Number(!values[8].trim()?0:values[8])
		};
	}

  @PrimaryColumn('character varying', { name: 'trip_id', length: 255 })
  tripId: string;

  @Column('character varying', { name: 'service_id', length: 255 })
  serviceId: string;

  @Column('character varying', { name: 'route_id', length: 255 })
  routeId: string;

  @Column('integer', { name: 'direction_id', nullable: true })
  directionId: number | null;

  @Column('character varying', {
    name: 'trip_type',
    nullable: true,
    length: 255,
  })
  tripType: string | null;

  @Column('character varying', {
    name: 'trip_headsign',
    nullable: true,
    length: 255,
  })
  tripHeadsign: string | null;

  @Column('character varying', {
    name: 'trip_short_name',
    nullable: true,
    length: 255,
  })
  tripShortName: string | null;

  @Column('integer', { name: 'wheelchair_accessible', nullable: true })
  wheelchairAccessible: number | null;

  @ManyToOne(() => Route, (route) => route.trips)
  @JoinColumn({name:"route_id",referencedColumnName:"routeId"})
  route: Route;

  @ManyToMany(() => CalendarDates)
  @JoinTable()
  calendarDates: Promise<CalendarDates[]>;

  @ManyToOne(() => Calendar, (calendar) => calendar.trips)
  @JoinColumn({name:"service_id",referencedColumnName:"serviceId"})
  calendar: Calendar;

  @OneToMany(() => StopTime, (stoptime) => stoptime.trip)
  stopTimes: StopTime[];
}