import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Stop } from './Stop.entity';

export enum PathwayMode {
  VOIE_PIETONNE = 1,
  ESCALIER = 2,
  TAPIS_ROULANT = 3,
  ESCALIER_MECANIQUE = 4,
  ASCENSEUR = 5,
  SAS_VALIDATION = 6,
}

export enum Direction {
  UNIDIRECTIONAL = 0,
  BIDIRECTIONAL = 1,
}

@Entity('pathways', { schema: 'public' })
export class Pathway {
  @PrimaryColumn('character varying', { name: 'pathways_id' })
  pathwaysId: string;

  @Column('character varying', { name: 'from_stop_id', nullable: true })
  fromStopId: string;

  @Column('character varying', { name: 'to_stop_id', nullable: true })
  toStopId: string;

  @Column({
    type: 'enum',
    enum: PathwayMode,
    default: PathwayMode.VOIE_PIETONNE,
    name: 'pathway_mode',
  })
  pathwayMode: PathwayMode;

  @Column({
    type: 'enum',
    enum: Direction,
    default: Direction.BIDIRECTIONAL,
    name: 'is_bidirectional',
  })
  isBidirectional: Direction;

  @Column('float', { name: 'length', nullable: true })
  length: number;

  @Column('integer', { name: 'transversal_time', nullable: true })
  transversalTime: number;

  @ManyToOne(() => Stop, (stop) => stop.fromPathway)
  @JoinColumn({name:"from_stop_id",referencedColumnName:"stopId"})
  fromStop: Stop;

  @ManyToOne(() => Stop, (stop) => stop.toPathway)
  @JoinColumn({name:"to_stop_id",referencedColumnName:"stopId"})
  toStop: Stop;
}
