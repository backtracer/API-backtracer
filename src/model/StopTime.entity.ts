import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Stop } from './Stop.entity';
import { Trip } from './Trip.entity';

@Entity('stop_times', { schema: 'public' })
export class StopTime {

  static fromCSVForTypeORM(values: string[]): Partial<StopTime> {
		return {
      tripId: !values[0]?.trim() ? null : values[0],
      arrivalTime: !values[1]?.trim() ? null : values[1],
      departureTime: !values[2]?.trim() ? null : values[2],
      stopId: !values[3]?.trim() ? null : values[3],
      stopSequence: parseInt(!values[4]?.trim() ? null : values[4]),
      pickupType: parseInt(!values[5]?.trim() ? null : values[5]),
      dropOffType: parseInt(!values[6]?.trim() ? null : values[6]),
      timepoint: parseInt(!values[9]?.trim() ? null : values[9]),
    };
	}


  @PrimaryColumn('character varying', { name: 'trip_id', length: 255 })
  tripId: string;

  @Column('character varying', { name: 'stop_id', length: 255 })
  stopId: string;

  @PrimaryColumn('integer', { name: 'stop_sequence' })
  stopSequence: number;

  @Column('character varying', {
    name: 'arrival_time',
    nullable: true,
    length: 9,
  })
  arrivalTime: string | null;

  @Column('character varying', {
    name: 'departure_time',
    nullable: true,
    length: 9,
  })
  departureTime: string | null;

  @Column('integer', { name: 'pickup_type', nullable: true })
  pickupType: number | null;

  @Column('integer', { name: 'drop_off_type', nullable: true })
  dropOffType: number | null;

  @Column('smallint', { name: 'timepoint', nullable: true })
  timepoint: number | null;

  @ManyToOne(() => Stop, (stop) => stop.stoptimes)
  @JoinColumn({name:"stop_id",referencedColumnName:"stopId"})
  stop: Stop;

  @ManyToOne(() => Trip, (trip) => trip.stopTimes)
  @JoinColumn({name:"trip_id",referencedColumnName:"tripId"})
  trip: Trip;
}
