import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Stop } from './Stop.entity';

@Entity('transfers', { schema: 'public' })
export class Transfer {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id: number;

  @Column('character varying', {
    name: 'from_stop_id',
    nullable: true,
    length: 255,
  })
  fromStopId: string | null;

  @Column('character varying', {
    name: 'to_stop_id',
    nullable: true,
    length: 255,
  })
  toStopId: string | null;

  @Column('integer', { name: 'min_transfer_time', nullable: true })
  minTransferTime: number | null;

  @ManyToOne(() => Stop, (stop) => stop.fromTransfer)
  @JoinColumn({name:"from_stop_id",referencedColumnName:"stopId"})
  fromStop: Stop;

  @ManyToOne(() => Stop, (stop) => stop.toTransfer)
  @JoinColumn({name:"to_stop_id",referencedColumnName:"stopId"})
  toStop: Stop;
}
