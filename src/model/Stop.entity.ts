import { StopTime } from './StopTime.entity';
import { Column, Entity, ManyToMany, OneToMany, PrimaryColumn } from 'typeorm';
import { Transfer } from './Transfer.entity';
import { Pathway } from './Pathway.entity';
import { Route } from './Route.entity';

@Entity()
export class Stop {
  static fromCSVForTypeORM(values: string[]): Partial<Stop> {
		return {
			stopId: values[0],
			stopName: values[2],
      stopLon: parseFloat(values[4]),
      stopLat: parseFloat(values[5]),
      zoneId: values[6],
      locationType: Number(values[8]),
      parentStation: values[9],
      wheelchairBoarding: Number(values[12]),
    };
	}


  @PrimaryColumn('character varying', { name: 'stop_id', length: 255 })
  stopId: string;

  @Column('character varying', { name: 'stop_name', length: 255 })
  stopName: string;

  @Column('numeric', { name: 'stop_lat', precision: 12, scale: 9 })
  stopLat: number;

  @Column('numeric', { name: 'stop_lon', precision: 12, scale: 9 })
  stopLon: number;

  @Column('character varying', { name: 'zone_id', nullable: true, length: 50 })
  zoneId: string | null;

  @Column('integer', { name: 'location_type', nullable: true })
  locationType: number | null;

  @Column('character varying', {
    name: 'parent_station',
    nullable: true,
    length: 255,
  })
  parentStation: string | null;

  @Column('integer', { name: 'wheelchair_boarding', nullable: true })
  wheelchairBoarding: number | null;

  @OneToMany(() => StopTime, (stoptimes) => stoptimes.stop)
  stoptimes: StopTime[];

  @OneToMany(() => Transfer, (transfer) => transfer.fromStop)
  fromTransfer: Transfer[];

  @OneToMany(() => Transfer, (transfer) => transfer.toStop)
  toTransfer: Transfer[];

  @OneToMany(() => Pathway, (pathway) => pathway.fromStop)
  fromPathway: Pathway[];

  @OneToMany(() => Pathway, (pathway) => pathway.toStop)
  toPathway: Pathway[];

  @ManyToMany(() => Route)
  routes: Promise<Route[]>;
}
