import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, PrimaryColumn } from 'typeorm';
import { Calendar } from './Calendar.entity';
import { Trip } from './Trip.entity';

@Entity('calendar_dates')
export class CalendarDates {
  static fromCSVForTypeORM(values: any[]): Partial<CalendarDates> {
    return {
      serviceId: values[0],
      date: new Date(values[1].substring(0,4),values[1].substring(4,6),values[1].substring(6)),
      exceptionType: values[2]
    };
  }

  @PrimaryColumn('character varying', {
    name: 'service_id',
    length: 255,
  })
  serviceId: string;

  @PrimaryColumn('character varying', { name: 'date' })
  date: Date;

  @Column('integer', { name: 'exception_type' })
  exceptionType: number;

  @ManyToMany(() => Trip)
  trips: Promise<Trip[]>;

  // @ManyToOne(() => Calendar, (calendar) => calendar.calendarDates)
  // @JoinColumn({name:"service_id",referencedColumnName:"serviceId"})
  // calendar: Calendar;
}
