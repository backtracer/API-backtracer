import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('stop_extensions', { schema: 'public' })
export class Transfer {
  @PrimaryColumn('character varying', { name: 'object_id', length: 255 })
  objectId: number;

  @Column('character varying', {
    name: 'object_system',
    nullable: true,
    length: 255,
  })
  objectSystem: string | null;

  @Column('character varying', {
    name: 'to_stop_id',
    nullable: true,
    length: 255,
  })
  objectCode: string | null;
}
