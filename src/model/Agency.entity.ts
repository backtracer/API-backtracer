import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm';
import { Route } from './Route.entity';

@Entity()
export class Agency {
	static fromCSVForTypeORM(values: string[]): Partial<Agency> {
		return {
			agencyId: values[0],
			agencyName: values[1],
		};
	}

	@PrimaryColumn('character varying', {
		name: 'agency_id',
		length: 255,
	})
	agencyId: string;

	@Column('character varying', { name: 'agency_name', length: 255 })
	agencyName: string;

	@OneToMany(() => Route, (route) => route.agency)
	routes: Route[];
}
