import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { Agency } from './Agency.entity';
import { Stop } from './Stop.entity';
import { Trip } from './Trip.entity';

@Entity()
export class Route {
  static fromCSVForTypeORM(values: any[]): Partial<Route> {
    return {
      routeId: values[0],
      agencyId: values[1],
      routeShortName: values[2],
      routeLongName: values[3],
      // route_desc skip no data
      routeType: values[5],
      // route_url ski no data
      routeColor: "#"+values[7],
      routeTextColor: "#"+values[8],
      // route_sort order skip no data
      shapes: null,
    };
  }

  @PrimaryColumn('character varying', { name: 'routeId', length: 255 })
  routeId: string;

  @Column('character varying', {
    name: 'agency_id',
    nullable: true,
    length: 255,
  })
  agencyId: string | null;

  @Column('character varying', {
    name: 'route_short_name',
    nullable: true,
    length: 255,
  })
  routeShortName: string | null;

  @Column('character varying', {
    name: 'route_long_name',
    nullable: true,
    length: 255,
  })
  routeLongName: string | null;

  @Column('integer', { name: 'route_type' })
  routeType: number;

  @Column('character varying', {
    name: 'route_color',
    nullable: true,
    length: 7,
  })
  routeColor: string | null;

  @Column('character varying', {
    name: 'route_text_color',
    nullable: true,
    length: 7,
  })
  routeTextColor: string | null;

  @Column('jsonb', {
    name: 'shapes',
    nullable: true,
  })
  shapes: any | null;

  @OneToMany(() => Trip, (trip) => trip.route)
  trips: Trip[];

  @ManyToOne(() => Agency, (agency) => agency.routes)
  @JoinColumn({name:"agency_id",referencedColumnName:"agencyId"})
  agency: Agency;

  @ManyToMany(() => Stop)
  @JoinTable()
  stops: Promise<Stop[]>;
}
