import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('route_type', { schema: 'public' })
export class RouteType {

  static fromCSVForTypeORM(values: any[]): Partial<RouteType> {
    return {
      routeType: values[0],
      otpType: values[1],
      routeTypeName: values[2],
      routeTypeDesc: values[3],
      routeTypeIdSvg: values[4],
    };
  }
  @PrimaryColumn('integer', { name: 'route_type' })
  routeType: number;

  @Column('character varying', {
    name: 'otp_type',
    nullable: true,
    length: 255,
  })
  otpType: string | null;

  @Column('character varying', {
    name: 'route_type_name',
    nullable: true,
    length: 255,
  })
  routeTypeName: string | null;

  @Column('character varying', {
    name: 'route_type_desc',
    nullable: true,
    length: 1023,
  })
  routeTypeDesc: string | null;

  @Column('character varying', {
    name: 'route_type_id_svg',
    nullable: true,
    length: 255,
  })
  routeTypeIdSvg: string | null;
}
