import { Module } from '@nestjs/common';
import { TransfersService } from './transfers.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Transfer } from '../model/Transfer.entity';

@Module({
	imports: [TypeOrmModule.forFeature([Transfer])],
  providers: [TransfersService],
	exports: [TransfersService]
})
export class TransfersModule {}
