import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Route } from 'src/model/Route.entity';
import { RouteType } from 'src/model/RouteType.entity';
import * as turf from '@turf/turf';
import {DeepPartial , FindManyOptions , FindOneOptions , getManager , IsNull , Not , Repository} from 'typeorm';

@Injectable()
export class RoutesService {
  constructor(
    @InjectRepository(Route)
    private routesRepository : Repository<Route> , 

    @InjectRepository(RouteType)
    private routeTypesRepository : Repository<RouteType> , 
  ) {}

  public saveOne(route : DeepPartial<Route>) : Promise<Route> {
    return this.routesRepository.save(route);
  }

  public async saveMany(routes : DeepPartial<Route>[]) : Promise<Route[]> {
    return this.routesRepository.save(routes);
  }

  public async getAll() {
    const findOptions : FindManyOptions<Route> = {}
    findOptions.where = {}
    findOptions.where.shapes = Not(IsNull())
    findOptions.relations = ['agency']
    findOptions.select = ["routeId" , "agencyId" , "routeShortName" , "routeLongName" , "routeType" , "routeColor" , "routeTextColor"]

    return await this.routesRepository.find(findOptions);
  }

  public async getById(id : string) : Promise<Route> {
    const findOptions : FindOneOptions = {}
    findOptions.relations = ['agency' , 'stops']
    return this.routesRepository.findOne(id , findOptions)
  }

  public async checkRouteType(routeType : number) : Promise<boolean> {
    const result = await this.routeTypesRepository.findOne(routeType);
    return result !== undefined && result !== null;
  }

  public async getIds() : Promise<DeepPartial<Route[]>>{
    return this.routesRepository.createQueryBuilder("route").select(['route.routeId']).getMany();
  }

  public async getTrainsPositions(routeId : string) : Promise<any>{
    const entityManager = getManager();
    let currentDate : Date = new Date()
    let hours : string = currentDate.getUTCHours() + 1 > 9 ? String(currentDate.getUTCHours() + 1) : "0"+(currentDate.getUTCHours() + 1)
    let minutes : string = currentDate.getUTCMinutes() > 9 ? String(currentDate.getUTCMinutes()) : "0" + currentDate.getMinutes()
    let seconds : string = currentDate.getUTCSeconds() > 9 ? String(currentDate.getUTCSeconds()) : "0" + currentDate.getUTCSeconds()
    let currentDateFormated : string =  hours + ":" + minutes + ":" + seconds;

    let request = "SELECT stGen.trip_id , stGen.stop_id , stGen.stop_sequence , stGen.departure_time , stGen.arrival_time , tr.direction_id , sGen.stop_name , sGen.stop_lat , sGen.stop_lon FROM stop_times stGen inner join stop sGen on (stGen.stop_id = sGen.stop_id)  "+
        " inner join (SELECT * from trip inner join calendar on (trip.service_id=calendar.service_id)  "+
        " WHERE calendar."+currentDate.toLocaleDateString("en-US" , {weekday : "long"}).trim().toLowerCase()+"='1' "+
        " AND trip_id in ( SELECT distinct st.trip_id FROM stop_times st WHERE trip_id in (select trip_id from trip where route_id='"+routeId+"') " +
        " GROUP BY trip_id HAVING '"+currentDateFormated+"' BETWEEN  MIN(departure_time) AND MAX(arrival_time) )" +
        " ) tr on (stGen.trip_id=tr.trip_id) ORDER BY trip_id ASC ; " ;
    let queryResults : {trip_id : string , stop_id : string , stop_sequence : number , departure_time : string , arrival_time : string , direction_id : number , stop_name : string , stop_lat : number , stop_lon : number}[] = await entityManager.query(request);


    let brutpositions = this.extractPositionFromStoptimes(queryResults , currentDate , currentDateFormated);
    let roundedPositions=[];
    let currentRoute = await this.getById(routeId);
    for (let position of brutpositions){
      roundedPositions.push(turf.nearestPointOnLine(currentRoute.shapes , turf.point([position.longitude , position.latitude])));
    }
    roundedPositions.forEach(element => {
      delete element["properties"]
    })
    return {type : "FeatureCollection" , features : roundedPositions};

  }

  private extractPositionFromStoptimes(results : {trip_id : string , stop_id : string , stop_sequence : number , departure_time : string , arrival_time : string , direction_id : number , stop_name : string , stop_lat : number , stop_lon : number}[] , currentDate : Date , currentDateFormated : string) : {latitude : number , longitude : number}[]{
    
    let map : Map<string , Map<number , {stop_sequence : number ,distance : number , stop_id : string , departure_time_seconds : number , arrival_time_seconds : number , direction_id : number , stop_name : string , stop_lat : number , stop_lon : number}>>= new Map<string , Map<number , {stop_sequence : number ,distance : number , stop_id : string , departure_time_seconds : number ,  arrival_time_seconds : number , direction_id : number , stop_name : string , stop_lat : number , stop_lon : number}>>();
    let nearestStopSequenceMap : Map<string , number>=new Map<string , number>();
    let currentDateInSeconds : number = (currentDate.getUTCHours() + 1) * 3600 + (currentDate.getUTCMinutes() * 60) + currentDate.getUTCSeconds();

    results.forEach(element => {
      let arrivalTimeSeconds : number = this.timeToSeconds(element.arrival_time)
      let departureTimeSeconds : number = this.timeToSeconds(element.departure_time)
      if(!map.has(element.trip_id)){
        let secondaryMap : Map<number , {stop_sequence : number ,distance : number , stop_id : string , departure_time_seconds : number ,  arrival_time_seconds : number , direction_id : number , stop_name : string , stop_lat : number , stop_lon : number}>= new Map<number , {stop_sequence : number ,distance : number , stop_id : string , departure_time_seconds : number ,  arrival_time_seconds : number , direction_id : number , stop_name : string , stop_lat : number , stop_lon : number}>();

        let currentDistance : number= Math.min(Math.abs(currentDateInSeconds-arrivalTimeSeconds) , Math.abs(currentDateInSeconds-departureTimeSeconds));

        secondaryMap.set(element.stop_sequence , {
          stop_sequence : element.stop_sequence ,
          distance : currentDistance , 
          stop_id : element.stop_id , 
          departure_time_seconds : departureTimeSeconds , 
          arrival_time_seconds : arrivalTimeSeconds , 
          direction_id : Number(element.direction_id) , 
          stop_name : element.stop_name , 
          stop_lat : Number(element.stop_lat) , 
          stop_lon : Number(element.stop_lon)
        });
        nearestStopSequenceMap.set(element.trip_id , element.stop_sequence);
        map.set(element.trip_id , secondaryMap);
      } else {
        let currentDistance : number= Math.min(Math.abs(currentDateInSeconds-arrivalTimeSeconds) , Math.abs(currentDateInSeconds-departureTimeSeconds));
        
        if(map.get(element.trip_id).get(nearestStopSequenceMap.get(element.trip_id)).distance>currentDistance){
          nearestStopSequenceMap.delete(element.trip_id)
          nearestStopSequenceMap.set(element.trip_id , element.stop_sequence)
        }
        map.get(element.trip_id).set(element.stop_sequence , {
          stop_sequence : element.stop_sequence ,
          distance : currentDistance , 
          stop_id : element.stop_id , 
          departure_time_seconds : departureTimeSeconds , 
          arrival_time_seconds : arrivalTimeSeconds , 
          direction_id : Number(element.direction_id) , 
          stop_name : element.stop_name , 
          stop_lat : Number(element.stop_lat) , 
          stop_lon : Number(element.stop_lon)
        })
      }
    });

    let positions : {latitude : number , longitude : number}[]=[]
    for(let key of map.keys()){
      let lastStop : {distance : number , stop_id : string , departure_time_seconds : number , arrival_time_seconds : number , direction_id : number , stop_name : string , stop_lat : number , stop_lon : number};
      let nextStop : {distance : number , stop_id : string , departure_time_seconds : number , arrival_time_seconds : number , direction_id : number , stop_name : string , stop_lat : number , stop_lon : number};

      if(map.get(key).get(nearestStopSequenceMap.get(key)).arrival_time_seconds <= currentDateInSeconds){
        lastStop = map.get(key).get(nearestStopSequenceMap.get(key));

        if(lastStop.departure_time_seconds>=currentDateInSeconds){// Train en gare
          nextStop = lastStop
        } else { //Train après son arrêt le plus proche
          nextStop = map.get(key).get(nearestStopSequenceMap.get(key)+1)
        }
      } else { //Train avant son arrêt le plus proche
        nextStop = map.get(key).get(nearestStopSequenceMap.get(key));
        if(map.get(key).has(nearestStopSequenceMap.get(key)-1)){
          lastStop = map.get(key).get(nearestStopSequenceMap.get(key)-1);
        } else {
          lastStop = map.get(key).get(0);
        }
      }
      if(lastStop === nextStop){
        positions.push({latitude : nextStop.stop_lat , longitude : nextStop.stop_lon});
      } else {

        let percentLastToNext : number = Math.abs((currentDateInSeconds-lastStop.departure_time_seconds)/(nextStop.arrival_time_seconds-lastStop.departure_time_seconds));
        let currentLatitude : number = lastStop.stop_lat+percentLastToNext * (nextStop.stop_lat-lastStop.stop_lat);
        let currentLongitude : number = lastStop.stop_lon+percentLastToNext * (nextStop.stop_lon-lastStop.stop_lon);
        positions.push({latitude : currentLatitude , longitude : currentLongitude})
      }

    }

    return positions;

  }

  private timeToSeconds(time : string) : number{
    let timeSplited : string[] = time.split(":");
    return (Number(timeSplited[0]) * 3600) + (Number(timeSplited[1]) * 60) + Number(timeSplited[2])
  }
    private secondsToTime(ts : number) : string{
    let hours = Math.floor(ts/3600)
    let remains = ts%3600
    let minutes = Math.floor(remains/60);
    let seconds =  remains%60;
    let hoursString : string = hours > 9 ? String(hours) : "0"+hours
    let minutesString : string = minutes > 9 ? String(minutes) : "0" + minutes
    let secondsString : string = seconds > 9 ? String(seconds) : "0" + seconds
    return hoursString+":"+minutesString+":"+secondsString
    
  }
}
