import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoutesService } from './routes.service';
import { RoutesController } from './routes.controller';
import { StopsModule } from 'src/stops/stops.module';
import { TripsModule } from 'src/trips/trips.module';
import { StopTimesModule } from 'src/stop-times/stop-times.module';
import { ShapesModule } from 'src/shapes/shapes.module';

import { Route } from 'src/model/Route.entity';
import { RouteType } from 'src/model/RouteType.entity';
import { CalendarsModule } from 'src/calendars/calendars.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Route, RouteType]),
    StopsModule,
    TripsModule,
    StopTimesModule,
    forwardRef(() => ShapesModule),
    CalendarsModule
  ],
  controllers: [RoutesController],
  providers: [RoutesService],
  exports: [RoutesService],
})
export class RoutesModule {}
