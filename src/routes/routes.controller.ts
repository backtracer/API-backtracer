import {
  Controller,
  Get,
  Param,
  Query,
} from '@nestjs/common';

import { RoutesService } from './routes.service';
import { TripsService } from '../trips/trips.service';
import { StopTimesService } from '../stop-times/stop-times.service';
import { Route } from 'src/model/Route.entity';

@Controller('routes')
export class RoutesController {
  constructor(
    private routesService: RoutesService,
    private tripsService: TripsService,
    private stopTimesService: StopTimesService,
  ) {}

  @Get()
  public async getAll() {
    return this.routesService.getAll();
  }

  @Get(':routeId')
  public async getByRouteId(@Param('routeId') routeId: string) {
    return await this.routesService.getById(routeId);
  }

  @Get(':routeId/stops')
  public async getStopsByRouteId(@Param('routeId') routeId: string) {
    const route: Route = await this.routesService.getById(routeId);
    return route.stops;
  }

  @Get(':routeId/trips')
  public async getStopsTimesByRouteId(
    @Param('routeId') routeId: string,
    @Query('full') full: boolean,
  ) {
    const trips = await this.tripsService.findByRouteId(routeId);

    if (full) {
      for (const trip of trips) {
        trip.stopTimes = await this.stopTimesService.findByTripId(trip.tripId);
      }
    }

    return trips;
  }

  @Get(':routeId/live')
  public async getTrainPositionByRouteId(
    @Param('routeId') routeId: string){
      return this.routesService.getTrainsPositions(routeId);

  }
}
