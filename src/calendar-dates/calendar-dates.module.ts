import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CalendarDates } from '../model/CalendarDates.entity';
import { CalendarDatesService } from './calendar-dates.service';

@Module({
	imports: [TypeOrmModule.forFeature([CalendarDates])],
	providers: [CalendarDatesService],
	exports: [CalendarDatesService]
})
export class CalendarDatesModule {}
