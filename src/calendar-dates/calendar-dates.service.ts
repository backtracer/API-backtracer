import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';
import { CalendarDates } from '../model/CalendarDates.entity';

@Injectable()
export class CalendarDatesService {
	constructor(
		@InjectRepository(CalendarDates) private calendarDatesRepository: Repository<CalendarDates>) {
	}

	async findByServiceId(searchedServiceId:string): Promise<CalendarDates[]>{
		return this.calendarDatesRepository.find({
			where: { serviceId: searchedServiceId },
		  });
	}
	async findByServiceIdAndDate(serviceId, date):Promise<CalendarDates>{
		return this.calendarDatesRepository.findOne({
			where: {serviceId: serviceId,date:date}
		});
	}

	saveOne(calendarDate: DeepPartial<CalendarDates>): Promise<CalendarDates> {
		return this.calendarDatesRepository.save(calendarDate);
	}

	async saveMany(calendarDates: DeepPartial<CalendarDates>[]): Promise<CalendarDates[]> {
		return this.calendarDatesRepository.save(calendarDates);
	}
}
