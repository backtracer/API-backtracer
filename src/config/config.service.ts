import { TypeOrmModuleOptions } from '@nestjs/typeorm';

require('dotenv').config();

class ConfigService {
  constructor(private env: { [k: string]: string | undefined }) {}

  private getValue(key: string, throwOnMissing = true): string {
    process.env
    const value = process.env[key];
    if (!value && throwOnMissing) {
      throw new Error(`config error - missing env.${key}`);
    }

    return value;
  }

  private getValueOrDefault(key: string, defaultValue = null): string {
    let value = process.env[key];
    if (!value) {
      value = defaultValue;
    }

    return value;
  }

  public ensureValues(keys: string[]) {
    keys.forEach((k) => this.getValue(k, true));
    return this;
  }

  public getPort() {
    return this.getValue('PORT', true);
  }

  public isProduction() {
    const mode = this.getValue('MODE', false);
    return mode != 'DEV';
  }

  public getTypeOrmConfig(): TypeOrmModuleOptions {
    return {
      type: 'postgres',

      host: this.getValueOrDefault("POSTGRES_HOST","db"), // this.getValue('POSTGRES_HOST'),
      port: parseInt(this.getValueOrDefault('POSTGRES_PORT',undefined)),
      username: this.getValueOrDefault("POSTGRES_USER","backtracer"), //this.getValue('POSTGRES_USER'),
      password: this.getValueOrDefault("POSTGRES_PASSWORD","backtracer"), //this.getValue('POSTGRES_PASSWORD'),
      database: this.getValueOrDefault("POSTGRES_DATABASE","backtracer"), //this.getValue('POSTGRES_DATABASE'),

      entities: ['dist/**/*.entity.js'],
      migrationsTableName: 'migration',

      migrations: ['src/migration/*.ts'],

      cli: {
        migrationsDir: 'src/migration',
      },
      synchronize: true,
      ssl: false,
      autoLoadEntities: true,
    };
  }
}

const configService = new ConfigService(process.env).ensureValues([
  'POSTGRES_HOST',
  'POSTGRES_PORT',
  'POSTGRES_USER',
  'POSTGRES_PASSWORD',
  'POSTGRES_DATABASE',
]);

export { configService };
