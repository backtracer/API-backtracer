import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Trip } from 'src/model/Trip.entity';
import { Repository, DeepPartial } from 'typeorm';

@Injectable()
export class TripsService {
  constructor(
    @InjectRepository(Trip)
    private tripsRepository: Repository<Trip>,
  ) {}

  public async findByRouteId(routeId: string): Promise<Trip[] | undefined> {
    return this.tripsRepository.find({ routeId });
  }
  public async findById(id: string): Promise<Trip | undefined> {
    return this.tripsRepository.findOne(id);
  }

  saveOne(trip: DeepPartial<Trip>): Promise<Trip> {
		return this.tripsRepository.save(trip);
	}

	async saveMany(trips: DeepPartial<Trip>[]): Promise<Trip[]> {
		return this.tripsRepository.save(trips);
	}
}
