import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Calendar } from 'src/model/Calendar.entity';
import { CalendarsService } from './calendars.service';



@Module({
    imports: [TypeOrmModule.forFeature([Calendar])],
    controllers: [],
    providers: [CalendarsService],
    exports: [CalendarsService],
  })
  export class CalendarsModule {}
  