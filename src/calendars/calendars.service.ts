import { Injectable } from '@nestjs/common';
import { DeepPartial, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Calendar } from '../model/Calendar.entity';

@Injectable()
export class CalendarsService {
	constructor(
		@InjectRepository(Calendar) private calendarRepository: Repository<Calendar>) {
	}

	async findById(id:string): Promise<Calendar>{
		return this.calendarRepository.findOne(id)
	}

	saveOne(calendar: DeepPartial<Calendar>): Promise<Calendar> {
		return this.calendarRepository.save(calendar);
	}

	async saveMany(calendars: DeepPartial<Calendar>[]): Promise<Calendar[]> {
		return this.calendarRepository.save(calendars);
	}
}
