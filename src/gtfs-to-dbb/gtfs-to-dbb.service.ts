import { Injectable } from '@nestjs/common';
import * as csv from '@fast-csv/parse';
import { Agency } from '../model/Agency.entity';
import { AgenciesService } from '../agencies/agencies.service';
import { Calendar } from '../model/Calendar.entity';
import { CalendarsService } from '../calendars/calendars.service';
import { CalendarDatesService } from '../calendar-dates/calendar-dates.service';
import { CalendarDates } from '../model/CalendarDates.entity';
import { RoutesService } from '../routes/routes.service';
import { Route } from '../model/Route.entity';
import { StopTimesService } from '../stop-times/stop-times.service';
import { StopTime } from '../model/StopTime.entity';
import { StopsService } from '../stops/stops.service';
import { Stop } from '../model/Stop.entity';
import { TripsService } from '../trips/trips.service';
import { Trip } from '../model/Trip.entity';
import { ShapesService } from '../shapes/shapes.service';
import { createQueryBuilder, QueryFailedError } from 'typeorm';
import { RouteType } from 'src/model/RouteType.entity';
import { RouteTypesService } from 'src/route-types/route-types.service';

export enum GTFSName {
	AGENCY = 'agency.txt',
	CALENDAR = 'calendar.txt',
	CALENDAR_DATES = 'calendar_dates.txt',
	ROUTES = 'routes.txt',
	STOP_TIMES = 'stop_times.txt',
	STOPS = 'stops.txt',
	TRANSFERS = 'transfers.txt',
	ROUTE_TYPES = 'route_types.txt',
	TRIPS = 'trips.txt',
}
const GTFSLineNumber = new Map;
GTFSLineNumber.set(GTFSName.AGENCY, 120);
GTFSLineNumber.set(GTFSName.CALENDAR, 690);
GTFSLineNumber.set(GTFSName.CALENDAR_DATES, 1497);
GTFSLineNumber.set(GTFSName.ROUTES, 1903);
GTFSLineNumber.set(GTFSName.STOP_TIMES, 9338314);
GTFSLineNumber.set(GTFSName.STOPS, 54174);
GTFSLineNumber.set(GTFSName.TRANSFERS, 205225);
GTFSLineNumber.set(GTFSName.TRIPS, 404747);

@Injectable()
export class GtfsToDbbService {
	counter:number = 0
	static agencyMap:Map<string,Agency> = new Map<string,Agency>();
	static routeMap:Map<string,Route> = new Map<string,Route>();
	static calendarMap:Map<string,Calendar> = new Map<string,Calendar>();
	static calendarDatesMap:Map<string,CalendarDates[]> = new Map<string,CalendarDates[]>();

	constructor(
		private agenciesService: AgenciesService,
		private calendarsService: CalendarsService,
		private calendarDatesService: CalendarDatesService,
		private routesService: RoutesService,
		private stopTimesService: StopTimesService,
		private routeTypesService: RouteTypesService,
		private stopsService: StopsService,
		private tripsService: TripsService,
		private shapesService: ShapesService,
	) {
	}

	async readCSV(gtfsName: GTFSName,nbmaxrecord:number) {
		return new Promise(async resolve => {
			let treated = 1
			console.log('Chargement de l\'élément du type ' + gtfsName);

			let firstLineReaded = false;

			let tempRecords:any[] = [];
			let recordsOk = 0;
			this.counter=0
			const parser = csv.parseFile(`${__dirname}/../../data/gtfs/${gtfsName}`, {
				trim: true,
				ignoreEmpty: true,
				headers: false,
			})
				.on('data', async (row) => {
					if (firstLineReaded) {
						const current_record = row;
						if (gtfsName === GTFSName.AGENCY) { //OK
							tempRecords.push(Agency.fromCSVForTypeORM(current_record));
						} else if (gtfsName === GTFSName.CALENDAR) { //OK 
							tempRecords.push(Calendar.fromCSVForTypeORM(current_record));
						} else if (gtfsName === GTFSName.CALENDAR_DATES) { //OK 
							const calendarDate = CalendarDates.fromCSVForTypeORM(current_record)
							if(calendarDate && calendarDate.serviceId){
								tempRecords.push(calendarDate);
							}
						} else if (gtfsName === GTFSName.ROUTES) { // OK 
							const route = Route.fromCSVForTypeORM(current_record)
							route.shapes = this.shapesService.getShapesByRouteId(route.routeId, route.routeType == 3);
							tempRecords.push(route);
						} else if (gtfsName === GTFSName.STOP_TIMES) { //OK
							const stopTime = StopTime.fromCSVForTypeORM(current_record);
							tempRecords.push(stopTime);
						} else if (gtfsName === GTFSName.ROUTE_TYPES) { //OK
							const routeType = RouteType.fromCSVForTypeORM(current_record);
							tempRecords.push(routeType);
						} else if (gtfsName === GTFSName.STOPS) { //OK
							tempRecords.push(Stop.fromCSVForTypeORM(current_record));
						} else if (gtfsName === GTFSName.TRANSFERS) {

						} else if (gtfsName === GTFSName.TRIPS) {
							const trip = Trip.fromCSVForTypeORM(current_record);
							tempRecords.push(trip);
							
						}

						if (tempRecords.length >= nbmaxrecord) {
							parser.pause();
							await this.pushRecordsToDatabase(tempRecords, gtfsName);
							recordsOk = recordsOk + tempRecords.length;
							tempRecords = [];
							// GtfsToDbbService.agencyMap.clear();

							parser.resume();
						}
					} else {
						firstLineReaded = true;
					}
				})
				.on('end', async () => {
					await this.pushRecordsToDatabase(tempRecords, gtfsName);
					resolve(true);
				});
				
		});
	}

	async pushRecordsToDatabase(records, gtfsName: GTFSName): Promise<void> {
		return new Promise(async resolve => {
			let currentid:string="";
			try{
				this.counter++;
				console.log("Enregistrement depuis "+gtfsName+" n°"+this.counter+" avec "+records.length+" éléments")
				if (gtfsName === GTFSName.AGENCY) {
					records.forEach(element => { currentid+=element.agencyId+";"});
					await this.agenciesService.saveMany(records);
				} else if (gtfsName === GTFSName.CALENDAR) {
					records.forEach(element => { currentid+=element.serviceId+";"});
					await this.calendarsService.saveMany(records);
				} else if (gtfsName === GTFSName.ROUTE_TYPES) {
					records.forEach(element => { currentid+=element.route_type+";"});
					await this.routeTypesService.saveMany(records);
				} else if (gtfsName === GTFSName.CALENDAR_DATES) {
					records.forEach(element => { currentid+=element.serviceId+"-"+element.date+";"});
					await this.calendarDatesService.saveMany(records);
				} else if (gtfsName === GTFSName.ROUTES) {
					records.forEach(element => { currentid+=element.routeId+";"});
					await this.routesService.saveMany(records);
				} else if (gtfsName === GTFSName.STOP_TIMES) {
					await this.stopTimesService.saveMany(records);
				} else if (gtfsName === GTFSName.STOPS) {
					records.forEach(element => { currentid+=element.stopId+";"});
					await this.stopsService.saveMany(records);
				} else if (gtfsName === GTFSName.TRANSFERS) {

				} else if (gtfsName === GTFSName.TRIPS) {
					records.forEach(element => { currentid+=element.tripId+";"});
					await this.tripsService.saveMany(records);
				}
			} catch (e) {
				console.log("Erreur lors de l'insertion du "+gtfsName+ " n°"+this.counter+" --> Essaie à un niveau plus precis")
				let subcounter=1
				
				for (let record of records){
					console.log("Enregistrement depuis "+gtfsName+" n°"+this.counter+"."+subcounter)
					subcounter++;
					try{
						if (gtfsName === GTFSName.AGENCY) {
							currentid=record.agencyId;
							await this.agenciesService.saveMany(records);
						} else if (gtfsName === GTFSName.CALENDAR) {
							currentid=record.serviceId;
							await this.calendarsService.saveMany(records);
						} else if (gtfsName === GTFSName.CALENDAR_DATES) {
							currentid=record.serviceId+"-"+record.date;
							await this.calendarDatesService.saveMany(records);
						} else if (gtfsName === GTFSName.ROUTE_TYPES) {
							records.forEach(element => { currentid+=element.route_type+";"});
							await this.routeTypesService.saveMany(records);
						} else if (gtfsName === GTFSName.ROUTES) {
							currentid=record.routeId;
							await this.routesService.saveMany(records);
						} else if (gtfsName === GTFSName.STOP_TIMES) {
							await this.stopTimesService.saveMany(records);
						} else if (gtfsName === GTFSName.STOPS) {
							currentid=record.stopId;
							await this.stopsService.saveMany(records);
						} else if (gtfsName === GTFSName.TRANSFERS) {

						} else if (gtfsName === GTFSName.TRIPS) {
							currentid=record.tripId;
							await this.tripsService.saveMany(records);
						}
					} catch(e) {
						console.log("Erreur lors de l'insertion du "+gtfsName+ " : n°"+this.counter+"."+subcounter+" avec "+currentid)
					}

				}
			}
			resolve();
		});
	}

	async postSript() {
		return new Promise(async resolve => {
			const listRoutes = await this.routesService.getIds();
			let i = 1
			for (const element of listRoutes){
				console.log("Préparation "+i+"/"+listRoutes.length+" --> "+element.routeId);
				i++;
				let route:Route = await this.routesService.getById(element.routeId);
				route.stops = this.stopsService.getStopsByRouteId(route.routeId);
				await this.routesService.saveOne(route);
				console.log("Ajout de "+(await route.stops).length+" sur la route "+route.routeId);
			}
		});
	}
}
