import { Module } from '@nestjs/common';
import { GtfsToDbbController } from './gtfs-to-dbb.controller';
import { GtfsToDbbService } from './gtfs-to-dbb.service';
import { AgenciesModule } from '../agencies/agencies.module';
import { CalendarsModule } from '../calendars/calendars.module';
import { CalendarDatesModule } from '../calendar-dates/calendar-dates.module';
import { RoutesModule } from '../routes/routes.module';
import { StopTimesModule } from '../stop-times/stop-times.module';
import { StopsModule } from '../stops/stops.module';
import { TripsModule } from '../trips/trips.module';
import { ShapesModule } from '../shapes/shapes.module';
import { RouteTypesModule } from 'src/route-types/route-types.module';

@Module({
	imports: [
    AgenciesModule,
    CalendarsModule,
    CalendarDatesModule,
    RoutesModule,
		StopTimesModule,
		StopsModule,
		TripsModule,
		ShapesModule,
		RouteTypesModule
  ],
	controllers: [GtfsToDbbController],
	providers: [GtfsToDbbService],
})
export class GtfsToDbbModule {
}
