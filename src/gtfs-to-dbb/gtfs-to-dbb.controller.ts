import { Controller, Get, Query } from '@nestjs/common';
import { ShapesService } from 'src/shapes/shapes.service';
import { GTFSName, GtfsToDbbService } from './gtfs-to-dbb.service';

@Controller('gtfs')
export class GtfsToDbbController {

	private scriptInProcess = false;

	constructor(private gtfsToDbbService: GtfsToDbbService) {
	}


	@Get()
	async run(@Query('agency') agency: number,
	@Query('calendar') calendar: number,
	@Query('calendarDate') calendarDate: number,
	@Query('route') route: number,
	@Query('trip') trip: number,
	@Query('stop') stop: number,
	@Query('stopTime') stopTime: number,
	@Query('transfer') transfer: number,
	@Query('routeTypes') routeTypes:number,
	@Query('postscript') postscript: number,
	) {
		if(!this.scriptInProcess){
			this.scriptInProcess=true;
			try {
				agency ? await this.gtfsToDbbService.readCSV(GTFSName.AGENCY,agency):console.log("Agencies Passées");
				
				calendar ?await this.gtfsToDbbService.readCSV(GTFSName.CALENDAR,calendar):console.log("Calendars passés");
				calendarDate ?await this.gtfsToDbbService.readCSV(GTFSName.CALENDAR_DATES,calendarDate):console.log("CalendarDates passés");

				//Need Agency_id in Agency
				if(route){
					console.log("Traitement des routes")
					ShapesService.initObjects();
					await this.gtfsToDbbService.readCSV(GTFSName.ROUTES,route);
					ShapesService.clearObjects();
				} else {
					console.log("Routes passées")
				}


				//Need Route_id in Route and service Id in calendar
				stop ? await this.gtfsToDbbService.readCSV(GTFSName.STOPS,stop) : console.log("Stops passées");
				trip ? await this.gtfsToDbbService.readCSV(GTFSName.TRIPS,trip) : console.log("Trips passées");
				routeTypes ? await this.gtfsToDbbService.readCSV(GTFSName.ROUTE_TYPES,routeTypes) : console.log("Route Types passées");
				
				//Need tripId and StopId 
				stopTime ? await this.gtfsToDbbService.readCSV(GTFSName.STOP_TIMES,stopTime) : console.log("StopTimes passés");

				//Need StopId
				transfer ? await this.gtfsToDbbService.readCSV(GTFSName.TRANSFERS,transfer) : console.log("Transfers passés");
				postscript ? await this.gtfsToDbbService.postSript() : console.log("Post Script passé");
				console.log('Fin !')
				this.scriptInProcess=false
			} catch (error) {
				this.scriptInProcess=false
				console.error(error);
			}
		} else {
			return "The script is in process"
		}
	}
}
