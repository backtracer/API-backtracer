import { Test, TestingModule } from '@nestjs/testing';
import { GtfsToDbbService } from './gtfs-to-dbb.service';

describe('GtfsToDbbService', () => {
  let service: GtfsToDbbService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GtfsToDbbService],
    }).compile();

    service = module.get<GtfsToDbbService>(GtfsToDbbService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
