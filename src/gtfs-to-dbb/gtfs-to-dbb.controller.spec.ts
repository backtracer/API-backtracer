import { Test, TestingModule } from '@nestjs/testing';
import { GtfsToDbbController } from './gtfs-to-dbb.controller';

describe('GtfsToDbbController', () => {
  let controller: GtfsToDbbController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GtfsToDbbController],
    }).compile();

    controller = module.get<GtfsToDbbController>(GtfsToDbbController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
