import { forwardRef, Module } from '@nestjs/common';
import { ShapesService } from './shapes.service';
import { RoutesModule } from '../routes/routes.module';

@Module({
  imports: [
    forwardRef(() => RoutesModule)
  ],
  providers: [ShapesService],
  controllers: [],
  exports: [ShapesService],
})
export class ShapesModule {}
