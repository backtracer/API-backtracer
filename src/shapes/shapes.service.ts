import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import { ReturningStatementNotSupportedError } from 'typeorm';

@Injectable()
export class ShapesService {
  static busFeatureMap:Map<string,any[]> = new Map<string,any[]>();
  static otherFeatureMap:Map<string,any[]> = new Map<string,any[]>();

  constructor() {}
  
  public static initObjects():void{
    this.initBus();
    this.initRail();
  }

  public static initBus():void{
    let obj = JSON.parse(
      fs.readFileSync(`${__dirname}/../../data/shapes_bus.geojson`, 'utf8'),
    );
    for (const line of obj.features) {
      if(!ShapesService.busFeatureMap.has(line)){
        ShapesService.busFeatureMap.set(line.properties.lineid,[line]);
      } else {
        ShapesService.busFeatureMap.get(line.properties.lineid).push(line)
      }
    }
  }

  public static initRail():void{
    let obj = JSON.parse(
      fs.readFileSync(`${__dirname}/../../data/shapes_railroad.geojson`, 'utf8'),
    );
    for (const line of obj.features) {
      if(!ShapesService.otherFeatureMap.has(line.properties.idrefligc)){
        ShapesService.otherFeatureMap.set(line.properties.idrefligc,[line]);
      } else {
        ShapesService.otherFeatureMap.get(line.properties.idrefligc).push(line)
      }
    }
  }

  public static clearObjects():void{
    ShapesService.busFeatureMap.clear();
    ShapesService.busFeatureMap.clear();
  }
  
  public getShapesByRouteId(routeId: string, isBus: boolean): any {
    const routeShortId = routeId.split(':')[routeId.split(':').length - 1];
    let value = isBus ? ShapesService.busFeatureMap.get(routeShortId) : ShapesService.otherFeatureMap.get(routeShortId)
    if(!value){
      return null;
    }
    const result = {} ;
    result['type'] = 'FeatureCollection';
    
    result['features'] = value;
    return result;
  }
}
