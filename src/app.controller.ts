import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): any {
    return { Application: "Backtracer", Module: "API-Backtracer", Documentation: "https://gitlab.com/backtracer/backtracer"};
  }
}
