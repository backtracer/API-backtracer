import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { configService } from './config/config.service';
import { RoutesModule } from './routes/routes.module';
import { TripsModule } from './trips/trips.module';
import { StopsModule } from './stops/stops.module';
import { StopTimesModule } from './stop-times/stop-times.module';
import { ShapesModule } from './shapes/shapes.module';
import { AgenciesModule } from './agencies/agencies.module';
import { CalendarsModule } from './calendars/calendars.module';
import { CalendarDatesModule } from './calendar-dates/calendar-dates.module';
import { PathwaysModule } from './pathways/pathways.module';
import { RouteTypesModule } from './route-types/route-types.module';
import { GtfsToDbbModule } from './gtfs-to-dbb/gtfs-to-dbb.module';
import { TransfersModule } from './transfers/transfers.module';
import { GeocodingModule } from './geocoding/geocoding.module';
import { ItineraryModule } from './itinerary/itinerary.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
    RoutesModule,
    TripsModule,
    StopsModule,
    StopTimesModule,
    ShapesModule,
    AgenciesModule,
    CalendarsModule,
    CalendarDatesModule,
    PathwaysModule,
    RouteTypesModule,
    GtfsToDbbModule,
    TransfersModule,
    GtfsToDbbModule,
    GeocodingModule,
    ItineraryModule
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
