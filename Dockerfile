FROM node:16.10-alpine 

WORKDIR /deployments

#COPY package.json package-lock.json ./
COPY package*.json ./

RUN npm i -g @nestjs/cli
RUN npm i

EXPOSE 3000